use rand::{seq::SliceRandom, SeedableRng};
use rand_chacha::ChaChaRng;
use std::ops::Deref;

use super::card::{all_cards, Card};
use super::fill_random;

const SEED_LEN: usize = 32;
const ENCODED_SEED_LEN: usize = 4 * ((SEED_LEN + 3 - 1) / 3); // 4 * ceil(SEED_LEN / 3)
pub type GameRng = ChaChaRng;

/// A `Deck` will always be shuffled according to the seed provided at initialization
#[derive(Clone, Copy, Debug)]
pub struct Deck {
    cards: [Card; 52],
    index: usize,
    pub seed: Seed,
}

/// We only compare the order of cards when comparing decks
impl std::cmp::PartialEq for Deck {
    fn eq(&self, other: &Self) -> bool {
        self.cards.eq(&other.cards)
    }
}

impl std::default::Default for Deck {
    fn default() -> Self {
        let seed = Seed::default();
        Self::new(seed)
    }
}

impl Deck {
    pub fn new(seed: Seed) -> Self {
        let mut cards = all_cards();
        let mut rng = ChaChaRng::from_seed(*seed);
        cards.shuffle(&mut rng);
        let d = Deck {
            cards,
            index: 0,
            seed,
        };
        d
    }

    pub fn can_draw(&self) -> bool {
        self.index < 52
    }

    /// Returns a card and increments the deck index
    /// # Panics
    /// Panics if index is out of bounds. i.e. this function is called 53 times on the same deck
    /// At that point the game is clearly in an unrecoverable state.
    pub fn draw(&mut self) -> Card {
        if self.index >= 52 {
            panic!("No cards left to draw!")
        }
        let c = self.cards[self.index];
        self.index += 1;
        c
    }

    pub fn burn(&mut self) {
        self.draw();
    }
    #[cfg(test)]
    #[allow(dead_code)]
    /// While running tests it's useful to have the raw deck order
    fn get_cards(self) -> [Card; 52] {
        self.cards
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Seed {
    value: [u8; SEED_LEN],
}

impl Deref for Seed {
    type Target = [u8; SEED_LEN];

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

impl std::default::Default for Seed {
    fn default() -> Seed {
        Seed {
            value: fill_random::<SEED_LEN>(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn test_same_seeds() {
        let d = Deck::default();
        let s = d.seed;
        let d2 = Deck::new(s);
        let d3 = Deck::new(s);
        assert_eq!(d, d2);
        assert_eq!(d, d3);
    }

    #[test]
    fn test_different_seeds() {
        let d = Deck::default();
        let d2 = Deck::default();
        let d3 = Deck::default();
        assert_ne!(d, d2);
        assert_ne!(d, d3);
    }

    /*#[test]
    fn all_cards_unique() {
        for _ in 0..1000 {
            let d = Deck::default();
            let c = d.get_cards();
            let mut c: Vec<Card> = c.into_iter().collect();
            // Obviously this looks dumb
            // Since PartialEq is implemented over only rank, we expect a result for each rank, not each rank + suit
            // As such, sort + dedup should always = number of ranks
            // A more proper test was harder than I had energy for
            // And it doesn't even work because I don't want Card = Eq
            //c.sort_unstable();
            c.dedup();
            assert_eq!(c.len(), 13);
        }
    }*/

    #[test]
    fn can_draw_52() {
        let mut d = Deck::default();
        for _ in 0..52 {
            d.draw();
        }
        assert_eq!(d.can_draw(), false)
    }

    #[test]
    #[should_panic]
    fn cannot_draw_53() {
        let mut d = Deck::default();
        for _ in 0..53 {
            d.draw();
        }
        // This line is never reached
    }
}
